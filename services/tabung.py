from db import Session
from sqlalchemy import exc
from fastapi.responses import JSONResponse
from fastapi import HTTPException
import models as md


class Tabung:
    session = Session()

    def __init__(self, no_rek, nominal):
        self.no_rek = no_rek
        self.nominal = nominal

    def tarik(self):
        rekening = self.get_rekening()
        if rekening.saldo <= 0 or rekening.saldo < self.nominal:
            raise HTTPException(status_code=400, detail="Saldo anda tidak cukup")
        rekening.saldo = rekening.saldo - self.nominal
        self.session.commit()
        self.session.refresh(rekening)
        self.create_mutasi("D", rekening.id)

        return rekening

    def tabung(self):
        rekening = self.get_rekening()

        rekening.saldo = rekening.saldo + self.nominal

        self.session.commit()
        self.session.refresh(rekening)
        self.create_mutasi("C", rekening.id)

        # print(rekening.saldo)
        return rekening

    def get_rekening(self):
        # rekk
        try:
            rek = (
                self.session.query(md.NoRekening)
                .filter(md.NoRekening.no_rek == self.no_rek)
                .one()
            )
        except exc.NoResultFound:
            raise HTTPException(status_code=400, detail="No rekening tidak ditemukan")
        return rek

    def create_mutasi(self, kode, rekening_id):
        self.session.add(
            md.Mutasi(
                kode_transaksi=kode, nominal=self.nominal, rekening_id=rekening_id
            )
        )
        self.session.commit()
