import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
import datetime
from sqlalchemy.orm import relationship

Base = declarative_base()


class Nasabah(Base):
    __tablename__ = "nasabah"

    id = sa.Column(sa.Integer, primary_key=True)
    nama = sa.Column(sa.String)
    nik = sa.Column(sa.String, unique=True)
    no_hp = sa.Column(sa.Integer, unique=True)


class NoRekening(Base):
    __tablename__ = "no_rekening"

    id = sa.Column(sa.Integer, primary_key=True)
    no_rek = sa.Column(sa.Integer, unique=True)
    saldo = sa.Column(sa.BigInteger, default=0, nullable=False)
    id_nasabah = sa.Column(sa.Integer)


class Mutasi(Base):
    __tablename__ = "mutasi"

    id = sa.Column(sa.Integer, primary_key=True)
    waktu = sa.Column(sa.DateTime, default=datetime.datetime.now())
    kode_transaksi = sa.Column(sa.String)
    rekening_id = sa.Column(sa.ForeignKey("no_rekening.id"))
    nominal = sa.Column(sa.BigInteger)
