from random import randint
from models import NoRekening
from db import Session


def get_random_num():
    min_ = 100
    max_ = 1000000000
    rand = randint(min_, max_)

    while (
        Session().query(NoRekening).filter(NoRekening.no_rek == rand).limit(1).first()
        is not None
    ):
        rand = randint(min_, max_)

    return rand
