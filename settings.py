import os
from dotenv import load_dotenv

# load_dotenv()

HOST = os.getenv("HOST", "localhost")
PORT = os.getenv("PORT", 8000)

DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")
DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_NAME = os.getenv("DB_NAME")
