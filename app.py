from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from schemas import CreateNasabah, CreateTabung
from db import Session
import models as md
from sqlalchemy import exc
import utils as ut
from services.tabung import Tabung
import json

app = FastAPI()


@app.exception_handler(HTTPException)
async def custom_exceiption(request, exc):
    return JSONResponse(
        status_code=exc.status_code, content={"remark": exc.detail}, headers=exc.headers
    )


# @app.get("/")
# def read_root():
#     return {"hello": "World"}


@app.post("/daftar", status_code=200)
def daftar(daftar: CreateNasabah):
    data = daftar.dict()

    data_res = {}
    with Session() as session:
        nasabah = md.Nasabah(
            no_hp=data.get("no_hp"), nama=data.get("nama"), nik=data.get("nik")
        )
        try:
            session.add(nasabah)
            session.commit()

            session.refresh(nasabah)

        except exc.IntegrityError:
            raise HTTPException(
                status_code=400,
                detail="Data NIK atau nomor handphone nasabah sudah terdaftar",
            )

        no_rek = md.NoRekening(no_rek=ut.get_random_num(), id_nasabah=nasabah.id)
        session.add(no_rek)
        session.commit()
        session.refresh(no_rek)

        data_res["nomor_rekening"] = no_rek.no_rek

    return JSONResponse(status_code=200, content=data_res)


@app.post("/tabung", status_code=200)
def tabung(tabung: CreateTabung):
    tabung_service = Tabung(tabung.no_rekening, tabung.nominal)
    rekening = tabung_service.tabung()

    return JSONResponse(status_code=200, content={"saldo": rekening.saldo})


@app.post("/tarik", status_code=200)
def tarik(tarik: CreateTabung):
    tabung_service = Tabung(tarik.no_rekening, tarik.nominal)
    rekening = tabung_service.tarik()
    return JSONResponse(status_code=200, content={"saldo": rekening.saldo})


@app.get("/saldo/{no_rekening}")
def saldo(no_rekening: int):
    data_res = {}
    with Session() as session:
        try:
            rek = (
                session.query(md.NoRekening)
                .filter(md.NoRekening.no_rek == no_rekening)
                .one()
            )
            data_res.update({"no_rekening": rek.no_rek, "saldo": rek.saldo})
        except exc.NoResultFound:
            raise HTTPException(status_code=400, detail="No rekening tidak ditemukan")

    return JSONResponse(content=data_res, status_code=200)


@app.get("/mutasi/{no_rekening}")
def mutasi(no_rekening: int):
    data_res = {}
    with Session() as session:
        try:
            mutasi = (
                session.query(md.Mutasi)
                .filter(
                    md.Mutasi.rekening_id == md.NoRekening.id,
                    md.NoRekening.no_rek == no_rekening,
                )
                .all()
            )
        except exc.NoResultFound:
            raise HTTPException(status_code=400, detail="Tidak ada data")

    return mutasi
