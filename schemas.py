from pydantic import BaseModel


class CreateNasabah(BaseModel):
    nama: str
    nik: str
    no_hp: int


class CreateTabung(BaseModel):
    no_rekening: int
    nominal: int


class SaldoCheck(BaseModel):
    no_rekening: int
