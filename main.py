from app import app
import uvicorn
import asyncio
import settings


async def main():
    uvicorn.run(
        app="main:app",
        host=settings.HOST,
        port=int(settings.PORT),
        reload=True,
        log_level="info",
    )


if __name__ == "__main__":
    asyncio.run(main())
